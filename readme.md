# Installation

 1. install *elasticsearch*
 2. install nutritionDb dependencies
 3. install web_app dependencies

# Usage

 1. run . /Users/wecii/Downloads/sources/nutrition/web_app/venv/bin/activate
 2. cd /Users/wecii/Downloads/sources/nutrition/web_app
 3. python3 app.py


## Details

 1. Two job scheduled in crontab to crawl data from turkomp
 2. Feeder is feeding elasticsearch from mysql tables