# -*- coding: utf-8 -*-
import scrapy
from scrapy.shell import inspect_response
from scrapy.http import Request
from nutrition.items import NutritionItem
import json
from urllib.parse import urlparse
import mysql.connector

class TurkompDetailSpider(scrapy.Spider):

    name = 'turkomp_detail'
    allowed_domains = ['turkomp.gov.tr']
    start_urls = []

    custom_settings = {
        'ITEM_PIPELINES': {
            'nutrition.pipelines.TurkompDetailPipeline': 300,
        }
    }

    def start_requests(self):

        con = mysql.connector.connect(host='localhost',
                                            user='mt', 
                                            password='15', 
                                            db='nutrition',
                                            charset="utf8",
                                            use_unicode=True)
        cursor = con.cursor()
        cursor.execute("""SELECT * FROM pages_to_scrape;""")
        dps = cursor.fetchall()
        for dp in dps:
            print(dp[1])
            print(dp[2])
            f = 'http://www.'+dp[1]+'/'+dp[2]
            print(f)
            yield scrapy.Request(url=f,callback=self.parse_item)

    def parse_item(self, response):
        print('getting response')
        prod = NutritionItem()
        prod['name'] = response.css('label.col-sm-12.control-label::text').extract_first()
        prod['calorie'] = response.xpath('//*[@id="foodResultlist"]/tbody/tr[1]/td[3]/a/text()').extract_first()
        prod['water'] = response.xpath('//*[@id="foodResultlist"]/tbody/tr[3]/td[3]/a/text()').extract_first()
        prod['fat'] = response.xpath('//*[@id="foodResultlist"]/tbody/tr[7]/td[3]/a/text()').extract_first()
        prod['carb'] = response.xpath('//*[@id="foodResultlist"]/tbody/tr[8]/td[3]/a/text()').extract_first()
        prod['prot'] = response.xpath('//*[@id="foodResultlist"]/tbody/tr[5]/td[3]/a/text()').extract_first()
        print(prod)
        yield prod
            
