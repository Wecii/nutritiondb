# -*- coding: utf-8 -*-
import scrapy
from scrapy.shell import inspect_response
from nutrition.items import NutritionUrlItem
import json
from urllib.parse import urlparse

class TurkompSpider(scrapy.Spider):
    name = 'turkomp'
    allowed_domains = ['turkomp.gov.tr']
    start_urls = ['http://www.turkomp.gov.tr/database']

    custom_settings = {
        'ITEM_PIPELINES': {
            'nutrition.pipelines.TurkompPipeline': 300,
        }
    }

    def parse(self, response):
        print('getting response')
        i = 0
        for tr_link in response.css('#mydatalist tbody tr'):
            i=i+1
            print('in for loop, turn : '+str(i))
            url = tr_link.css('a.link::attr(href)').extract_first()
            prod = NutritionUrlItem()
            prod['domain'] = 'turkomp.gov.tr'
            prod['url'] = url
            yield prod
            
