# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import sys
import hashlib
from scrapy.exceptions import DropItem
from scrapy.http import Request
import mysql.connector

class NutritionPipeline(object):
    def process_item(self, item, spider):
        return item

class TurkompDetailPipeline(object):

    '''
    create table details_to_scrape ( id int AUTO_INCREMENT PRIMARY KEY, name varchar(255) not null, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, calorie varchar(255), fat varchar(255), carb varchar(255), prot varchar(255), water varchar(255)) ENGINE=INNODB;
    '''

    def __init__(self):
        print('creating connection')
        self.conn = mysql.connector.connect(host='localhost',
                                            user='mt', 
                                            password='15', 
                                            db='nutrition',
                                            charset="utf8",
                                            use_unicode=True)
        print('connection successfull')
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        print('inserting to table details_to_scrape')
        try:
            self.cursor.execute("""INSERT INTO details_to_scrape (name,calorie,water,fat,carb,prot)  
                        VALUES (%s, %s, %s, %s, %s, %s)""", 
                       (item['name'].encode('utf-8'), 
                        item['calorie'].encode('utf-8'), 
                        item['water'].encode('utf-8'), 
                        item['fat'].encode('utf-8'), 
                        item['carb'].encode('utf-8'), 
                        item['prot'].encode('utf-8')))
            print('inserting to table details_to_scrape successfull')         
            self.conn.commit()
            self.cursor.close()
            self.conn.close()
        except Exception as e:
            print(e)     

class TurkompPipeline(object):

    '''
    create table pages_to_scrape ( id int AUTO_INCREMENT PRIMARY KEY, domain varchar(255), url varchar(1024)) ENGINE=INNODB;
    '''

    def __init__(self):
        print('creating connection')
        self.conn = mysql.connector.connect(host='localhost',
                                            user='mt', 
                                            password='15', 
                                            db='nutrition',
                                            charset="utf8",
                                            use_unicode=True)
        print('connection successfull')
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):
        print('inserting to table pages_to_scrape')
        try:
            self.cursor.execute("""INSERT INTO pages_to_scrape (domain, url)  
                        VALUES (%s, %s)""", 
                       (item['domain'].encode('utf-8'), 
                        item['url'].encode('utf-8')))
            print('inserting to table pages_to_scrape successfull')         
            self.conn.commit()
            self.cursor.close()
            self.conn.close()
        except Exception as e:
            print(e)            
