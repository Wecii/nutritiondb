from datetime import datetime
from flask import Flask, jsonify, request
from elasticsearch import Elasticsearch

es = Elasticsearch()
app = Flask(__name__)


@app.route("/index", methods=['GET'])
def index():
    results = es.search(index='nutrition', body={"query":{"match_all":{}}})
    return jsonify(results)


@app.route('/index', methods=['POST'])
def insert_data():

    json = request.get_json()
    body = json['data']
    data_type = json['data_type']
    data_id = json['data_id']
    result = es.index(index='nutrition', doc_type=data_type, id=data_id, body=body)

    return jsonify(result)


@app.route('/search', methods=['POST'])
def search():
    body = request.get_json()
    keyword = body['keyword']
    fields = body['fields']
    data_index = body['data_index']
    data_type = body['data_type']

    print(keyword)
    print(fields)
    bodyQ = {
        "query": {
            "match":{
                "name":keyword
            }
        }
    }

    res = es.search(index=data_index, body=bodyQ)

    return jsonify(res)


if __name__ == "__main__":
    app.run(debug=True)
