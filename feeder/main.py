import mysql.connector
import json
import requests

print('creating connection')
conn = mysql.connector.connect(host='localhost',user='mt', password='15', db='nutrition',charset="utf8",use_unicode=True)
print('connection successfull')
cursor = conn.cursor()

cursor.execute("""SELECT * FROM details_to_scrape;""")
dps = cursor.fetchall()
dataset = []
for dp in dps:
    data = {}
    data['data_type'] = 'food'
    data['data_id'] = 'nutrition_food'

    detail = {}
    detail['name'] = dp[1]
    detail['calorie'] = dp[3]
    detail['water'] = dp[4]
    detail['fat'] = dp[5]
    detail['carb'] = dp[6]
    detail['prot'] = dp[7]

    data['data'] = detail

    dataset.append(data)

cursor.close()
conn.close()


for data in dataset:
    r = requests.post('http://127.0.0.1:5000/index', json=data)
    print(r.status_code, r.reason)